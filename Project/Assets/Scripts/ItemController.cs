﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemController : MonoBehaviour {

    public Transform orignalParent;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter(Collider slot)
    {
        transform.parent.parent.GetComponent<InventoryController>().selectedSlot = slot.transform;
    }


    public void ItemSelected()
    {
        transform.parent.parent.GetComponent<InventoryController>().selectedItem = this.transform;
        transform.parent.parent.GetComponent<InventoryController>().orignalSlot = this.transform.parent.transform;
        transform.parent.parent.GetComponent<InventoryController>().orignalSlot.SetAsLastSibling();
    }

    public void ItemDeselected()
    {
        Transform selectedItem = transform.parent.parent.GetComponent<InventoryController>().selectedItem;
        Transform orignalSlot = transform.parent.parent.GetComponent<InventoryController>().orignalSlot;

        Transform selectedtSlot = transform.parent.parent.GetComponent<InventoryController>().selectedSlot;

        if (selectedItem != null &&
            orignalSlot != null &&
            selectedtSlot != null && orignalSlot != selectedtSlot)
        {

            PlayerInventory inventory = GameObject.Find("Alpha").GetComponent<PlayerInventory>();
            int indexA = orignalSlot.GetComponent<SlotController>().Index;
            int indexB = selectedtSlot.GetComponent<SlotController>().Index;

            if (inventory.isItemSlotEmpty(indexA) != true)
            {
                if (inventory.canSwapItems(indexA, indexB))
                {
                    inventory.swapItems(indexA, indexB);
                    Sprite temp;

                    selectedItem.position = orignalSlot.position;
                    temp = selectedItem.GetComponent<Image>().sprite;
                    selectedItem.GetComponent<Image>().sprite = selectedtSlot.GetChild(0).GetComponent<Image>().sprite;
                    selectedtSlot.GetChild(0).GetComponent<Image>().sprite = temp;
                }
            }
        }

        selectedItem.SetParent(selectedItem.GetComponent<ItemController>().orignalParent);
        transform.position = transform.parent.GetComponent<SlotController>().imageOriginalLocation.position;
        transform.parent.parent.GetComponent<InventoryController>().selectedItem = null;
        transform.parent.parent.GetComponent<InventoryController>().orignalSlot = null;
        transform.parent.parent.GetComponent<InventoryController>().selectedSlot = null;
    }

    public void Movetem()
    {
        //Debug.Log("Mouse moved in");
        //Debug.Log(transform.parent.parent.GetComponent<InventoryController>().selectedItem);
        if (transform.parent.parent.GetComponent<InventoryController>().selectedItem != null)
        {
            //transform.parent.parent.GetComponent<InventoryController>().selectedItem.SetParent(transform.parent);
            //transform.parent.parent.GetComponent<InventoryController>().selectedItem.SetAsFirstSibling();
            transform.parent.parent.GetComponent<InventoryController>().selectedSlot = this.transform.parent.transform;
           // Debug.Log("Came in");
        }
        
    }

}
