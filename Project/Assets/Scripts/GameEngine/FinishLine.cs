﻿using UnityEngine;
using System.Collections;

public class FinishLine : MonoBehaviour {

    public GameObject gameManager;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        gameManager.GetComponent<GameManager>().TriggerEntered(col);
    }
}
