﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    private float timeLimit= 300;
    private bool gameOver = false;
    private GameObject timer;
    // Use this for initialization
    void Start () {

        timer = GameObject.Find("Timer");
    }
	
	// Update is called once per frame
	void Update () {
        timeLimit -= Time.deltaTime;

        if (timeLimit > 0.0f && gameOver == false)
        {
            string countDown = string.Format("{0}:{1:00}", (int)timeLimit / 60, (int)timeLimit % 60);
            timer.GetComponent<Text>().text = "Time Limit: " + countDown;
        }
        else
        {
            gameOver = true;
            timer.GetComponent<Text>().text = "Game Over";
        }
	}

    public void TriggerEntered(Collider playerCollider)
    {
        Debug.Log("YOU WIN!");
        gameOver = true;
    }
}
