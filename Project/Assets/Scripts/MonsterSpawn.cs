﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MonsterSpawn : NetworkBehaviour  {

    [SerializeField] GameObject zombiePrefab;
    [SerializeField] GameObject zombieSpawn;

    private int counter;
    private int numberOfMonsters = 1;

    public override void OnStartServer()
    {
        for (int i = 0; i < numberOfMonsters; i++)
        {
            SpawnZombies();
        }
    }

    void SpawnZombies()
    {
        counter++;
        GameObject go = GameObject.Instantiate(zombiePrefab, zombieSpawn.transform.position, Quaternion.identity) as GameObject;
        NetworkServer.Spawn(go);
        go.GetComponent<Monster_ID>().monsterId = "Monster" + counter;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
