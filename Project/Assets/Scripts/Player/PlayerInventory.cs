﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
    Description: Class for player inventory 
    Does Item Management
*/

public class PlayerInventory : MonoBehaviour
{ 
    private Item[] items;

    //determines Inventory size limit
    private int inventorySize = 4;
    private GameObject invUI;

    // Use this for initialization 
    void Start()
    {
        items = new Item[inventorySize];
        invUI = GameObject.Find("InventoryUI");
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Add item to player inventory
    public void AddItem(Item item)
    {
        int startIndex = 0;
        if (item.itemType.Equals("Equipment") != true)
        {
            startIndex = 1;
        }

        for (int i = startIndex; i < inventorySize; i++)
        {
            if (items[i] == null)
            {
                items[i] = item;
                //Debug.Log("Item: " + item.itemName + " Added to slot " + i);
                invUI.GetComponent<InventoryController>().updateSlotImage(i, item.itemType);

                break;
            }
        }


    }

    //method to check if inventory is full
    public bool IsInventoryFull()
    {
        bool full = true;
        for (int i = 0; i < inventorySize; i++)
        {
            if (items[i] == null)
            {
                full = false;
                break;
            }
        }

        return full;
    }

    //method to receieve the inventory list
    public Item[] getItemList ()
    {
        return items;
    }

    public void removeItem(int index)
    {
       // Debug.Log(index);
       // Debug.Log(items[index].itemName + "removed");
        items[index] = null;

        invUI.GetComponent<InventoryController>().updateSlotImage(index, "null");
    }

    public void swapItems (int item1Index, int item2Index)
    {
        Item temp = items[item1Index];
        items[item1Index] = items[item2Index];
        items[item2Index] = temp;
    }

    public bool canSwapItems (int slotAIndex, int slotBIndex)
    {
        //check to see if they are trying to move items to the On-Hand

            if (slotAIndex == 0 || slotBIndex == 0)
            {
                if (slotAIndex == 1 || slotBIndex == 1)
                {
                    if (items[1] == null)

                        return false;

                    else if (items[1].itemType.Equals("Equipment") != true)
                        return false;
                }
                else if (slotAIndex == 2 || slotBIndex == 2)
                {
                    if (items[2] == null)

                        return false;

                    else if (items[2].itemType.Equals("Equipment") != true)
                        return false;
                }
            else 
            {
                if (items[3] == null)

                    return false;

                else if (items[3].itemType.Equals("Equipment") != true)
                    return false;
            }

        }
            return true;
    }

    public bool isItemSlotEmpty(int index)
    {
        bool result = false;
        if (items[index] == null)
            result = true;
        return result;
    }
    
}
