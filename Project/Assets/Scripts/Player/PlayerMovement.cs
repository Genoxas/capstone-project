﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    Animator anim;
    private float vertAxis;
    private bool stop = false;
    public bool inventoryFull;
    private float stamina;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        stamina = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (anim != null)
        {
            if (stop == false)
            {
                if (stamina < 100 && Input.GetKey(KeyCode.LeftShift) == false)
                {
                    stamina = stamina + 0.5f;
                    GameObject staminaUI = GameObject.Find("StaminaText");
                    Text staminaText = staminaUI.GetComponent<Text>();
                    staminaText.text = string.Format("Stamina : {0}%", Mathf.Round(stamina));
                }
                if (Input.GetKey(KeyCode.W))
                {
                    setRotation(0);
                    addSpeed(0.5f);
                    sprint();
                }
                else if (Input.GetKey(KeyCode.S))
                {
                    setRotation(180);
                    addSpeed(0.5f);
                    sprint();
                }
                else if (Input.GetKey(KeyCode.A))
                {
                    setRotation(270);
                    addSpeed(0.5f);
                    sprint();
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    setRotation(90);
                    addSpeed(0.5f);
                    sprint();
                }
                else
                {
                    vertAxis = 0;
                    anim.SetFloat("Speed", vertAxis);
                }
            }
        }
    }

    void sprint()
    {
        if (stamina > 0)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                addSpeed(1);
                transform.Translate(Vector3.forward * (Time.deltaTime * 2));
                GameObject staminaUI = GameObject.Find("StaminaText");
                stamina -= 0.5f;
                Text staminaText = staminaUI.GetComponent<Text>();
                staminaText.text = string.Format("Stamina : {0}%", Mathf.Round(stamina));
            }
        }
    }
    void setRotation(float rotateNum)
    {
        transform.rotation = Quaternion.Euler(0, rotateNum, 0);
        transform.Translate(Vector3.forward * Time.deltaTime);
    }

    public void addSpeed(float speed)
    {
        anim.SetFloat("Speed", speed);
    }

    public void pausePlayer()
    {
        stop = true;
    }

    public void unpausePlayer()
    {
        stop = false;
    }
}
