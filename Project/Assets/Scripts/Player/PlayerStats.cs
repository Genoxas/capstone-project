﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{

    private float playerHealth = 100;
    private float playerStamina = 100;

    // Use this for initialization
    void Start()
    {
        GameObject.Find("ScreenUI/HpText").GetComponent<Text>().text = string.Format("Health : {0}%", Mathf.Round(playerHealth)); ;
        //GameObject.Find("ScreenUI/StaminaText").GetComponent<Text>().text = string.Format("Stamina : {0}%", Mathf.Round(playerStamina)); ;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("ScreenUI/HpText").GetComponent<Text>().text = string.Format("Health : {0}%", Mathf.Round(playerHealth));
        //GameObject.Find("ScreenUI/StaminaText").GetComponent<Text>().text = string.Format("Stamina : {0}%", Mathf.Round(playerStamina)); 
    }

    public void registerHit(float damageValue)
    {
        if (playerHealth - damageValue < 0)
        {
            playerHealth = 0;
        }
        else
        {
            playerHealth -= damageValue;
        }
        Debug.Log(playerHealth);
        if (playerHealth==0)
        {
            this.GetComponent<Animator>().SetBool("Death", true);
            this.GetComponent<CapsuleCollider>().enabled = false;
            this.GetComponent<PlayerMovement>().pausePlayer();
        }
    }
    public float getHealth()
    {
        return playerHealth;
    }
}
