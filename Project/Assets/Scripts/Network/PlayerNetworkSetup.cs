﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class PlayerNetworkSetup : NetworkBehaviour {

    [SerializeField] Camera camera; 
   // [SerializeField] AudioListener al;

	// Use this for initialization
	void Start () {
        if (isLocalPlayer)
        {
            //GameObject.Find("Main Camera").SetActive(false);

            //GetComponent<CharacterController>().enabled = true;
            GetComponent<PlayerMovement>().enabled = true;
            GetComponent<PlayerInventory>().enabled = true;
            GetComponent<PlayerStats>().enabled = true;
            GetComponent<IKSystem>().enabled = true;
            //al.enabled = true;
            
            camera.enabled = true;
            camera.transform.parent = null;
        }    
	}
}
