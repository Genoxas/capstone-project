﻿using UnityEngine;
using System.Collections;

public class PickUpItem : MonoBehaviour
{
    private GameObject[] playerArray;
    private bool isPicked;
    // Use this for initialization
    void Start()
    {
        if (playerArray == null)
        {
            playerArray = GameObject.FindGameObjectsWithTag("Player");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isPicked == false)
        {
            foreach (GameObject player in playerArray)
            {
                float distance = Vector3.Distance(player.transform.FindChild("Alpha:Hips/Alpha:Spine/Alpha:Spine1/Alpha:Spine2/Alpha:RightShoulder/Alpha:RightArm/Alpha:RightForeArm/Alpha:RightHand").position, transform.position);
                Debug.Log(distance);
                if (distance < 0.8f && Input.GetKeyDown(KeyCode.E))
                {
                    Debug.Log(distance);
                    player.GetComponent<IKSystem>().rightHandObj = this.gameObject.transform;
                    player.GetComponent<IKSystem>().lookObj = this.gameObject.transform;
                    {
                        if (player.GetComponent<PlayerInventory>().IsInventoryFull() == false)
                        {
                            player.GetComponent<Animator>().SetBool("GetItem", true);
                            player.GetComponent<PlayerMovement>().addSpeed(0);
                        }
                    }
                }
                if (distance < 0.1f)
                {
                    isPicked = true;
                    player.GetComponent<Animator>().SetBool("GetItem", false);
                    transform.parent = player.transform.FindChild("Alpha:Hips/Alpha:Spine/Alpha:Spine1/Alpha:Spine2/Alpha:RightShoulder/Alpha:RightArm/Alpha:RightForeArm/Alpha:RightHand");
                    Item item = this.GetComponent<Item>();
                    player.GetComponent<PlayerInventory>().AddItem(item);
                }
            }
        }
    }
}