﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour {

    public Transform selectedItem;
    public Transform selectedSlot;
    public Transform orignalSlot;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetMouseButton(0) && selectedItem != null)
        {
            selectedItem.position = Input.mousePosition;
            //if (selectedItem.position.x )
        }
        /*
        else if (Input.GetMouseButtonUp(0) && selectedItem != null)
        {
            selectedItem.parent = selectedSlot;
            selectedItem.localPosition = Vector3.zero;
        }
        */
        
	}

    public void updateSlotImage (int index, string type)
    {
        
        if (index == 0)
        {
            GameObject itemImage = GameObject.Find("ItemImage0");
            Image itemComp = itemImage.GetComponent<Image>();

            // itemImage.SetActive(false);
            determineImage(type, itemImage);
            itemComp.sprite = determineImage(type, itemImage);
            
        }
        else if (index == 1)
        {
            GameObject itemImage = GameObject.Find("ItemImage1");
            Image itemComp = itemImage.GetComponent<Image>();

            // itemImage.SetActive(false);
            determineImage(type, itemImage);
            itemComp.sprite = determineImage(type, itemImage);
        }
        else if (index == 2)
        {
            GameObject itemImage = GameObject.Find("ItemImage2");
            Image itemComp = itemImage.GetComponent<Image>();

            // itemImage.SetActive(false);
            determineImage(type, itemImage);
            itemComp.sprite = determineImage(type, itemImage);
        }
        else
        {
            GameObject itemImage = GameObject.Find("ItemImage3");
            Image itemComp = itemImage.GetComponent<Image>();

            // itemImage.SetActive(false);
            determineImage(type, itemImage);
            itemComp.sprite = determineImage(type, itemImage);
        }
    }

    private Sprite determineImage (string type, GameObject itemImage)
    {
        if (type.Equals("Equipment"))
        {
            return itemImage.GetComponent<SpriteController>().Sword;
        }
        else if (type.Equals("PuzzleItem"))
        {
            return null;
        }

        return itemImage.GetComponent<SpriteController>().UIMask;
    }

}
