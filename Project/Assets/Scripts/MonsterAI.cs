﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterAI : MonoBehaviour
{
    private GameObject[] monsterPaths;
    private GameObject[] players;
    private GameObject lastFoundPlayer;
    private GameObject chosenPath;
    private NavMeshAgent monster;
    private float destination;
    private float playerDistance;
    private float aggroTimer = 6;
    private float playerImmunityTimer = 12;
    private float foundPlayerDistance;
    private float monsterAttackCooldown = 0;
    private int storedPlayerID;
    private int indexOfPath;
    private bool foundInitialPlayer = false;
    private RaycastHit findPlayer;
    private monsterState monsterCurrentState = monsterState.onPath;

    // Use this for initialization
    void Start()
    {
        monster = GetComponent<NavMeshAgent>();
        monsterPaths = GameObject.FindGameObjectsWithTag("MonsterPathing");
        indexOfPath = Random.Range(0, monsterPaths.Length);
        monster.SetDestination(monsterPaths[indexOfPath].transform.position);
        chosenPath = monsterPaths[indexOfPath];
        players = GameObject.FindGameObjectsWithTag("Player");
    }

    //enum which determines the monsters current status
    private enum monsterState
    {
        onPath = 0,
        foundNewPlayer = 1,
        foundNewPath = 2
    };

    // Update is called once per frame
    void Update()
    {
        Debug.Log(players.Length);
        //Perform a switch on the enum declared above to check the current state of the monster
        switch (monsterCurrentState)
        {
            case (monsterState.onPath):
                {
                    //When the monsters current position is close to the current path block by 2 blocks then find another path
                    if ((monster.transform.position - chosenPath.transform.position).magnitude < 2)
                    {
                        indexOfPath = Random.Range(0, monsterPaths.Length);
                        monster.SetDestination(monsterPaths[indexOfPath].transform.position);
                        chosenPath = monsterPaths[indexOfPath];
                    }
                    //Find all players in the players array and locate distance between monster and player
                    foreach (GameObject player in players)
                    {
                        playerDistance = Vector3.Distance(this.transform.position, player.transform.position);
                        //if distance is under 8 blocks then execute the following
                        if (playerDistance < 8)
                        {
                            //executes when the monster first locates a player and sets the unique player instance id/gameobject as well as the monster state
                            if (foundInitialPlayer == false)
                            {
                                storedPlayerID = player.GetInstanceID();
                                lastFoundPlayer = player;
                                monsterCurrentState = monsterState.foundNewPlayer;
                                foundInitialPlayer = true;
                            }
                            //executes when a second player is found after the fact a monster has found the first player. Overwrites the existing unique instance id/gameobject for monster and sets monster state
                            else if (foundInitialPlayer == true && storedPlayerID != player.GetInstanceID())
                            {
                                storedPlayerID = player.GetInstanceID();
                                lastFoundPlayer = player;
                                monsterCurrentState = monsterState.foundNewPlayer;
                                break;
                            }
                        }
                    }
                    //while monster is still on monster path, if player is found in the monsters sights he will set his current state to found player and continue to chase
                    if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out findPlayer, 10))
                    {
                        if (findPlayer.transform.tag == "Player")
                        {
                            storedPlayerID = findPlayer.transform.GetInstanceID();
                            lastFoundPlayer = findPlayer.transform.gameObject;
                            monsterCurrentState = monsterState.foundNewPlayer;
                        }
                    }
                    //if monster can still sense old player after another new player is found then give him immunity for 12 seconds and when timer hits 0 then make him able to reaggro player again
                    if (foundInitialPlayer == true && playerImmunityTimer >= 0)
                    {
                        playerImmunityTimer -= Time.deltaTime;
                    }
                    else if (playerImmunityTimer < 0)
                    {
                        foundInitialPlayer = false;
                        playerImmunityTimer = 12;
                    }
                    break;
                }
            case (monsterState.foundNewPlayer):
                {
                    //if a new player is found the keep setting the destination of the monster to the player but also - time from the aggro timer
                    monster.SetDestination(lastFoundPlayer.transform.position);
                    aggroTimer -= Time.deltaTime;
                    foundPlayerDistance = Vector3.Distance(this.gameObject.transform.position, lastFoundPlayer.transform.position);
                    //if monster and player are two blocks close to eachother then damage player. Once attacked monster attack will be on cooldown.
                    if (monsterAttackCooldown >= 0)
                    {
                        monsterAttackCooldown -= Time.deltaTime;
                    }
                    else if (foundPlayerDistance < 2 && monsterAttackCooldown < 0)
                    {
                        Debug.Log("Hit Player");
                        monsterAttackCooldown = 2;
                        lastFoundPlayer.GetComponent<PlayerStats>().registerHit(50);
                        if (lastFoundPlayer.GetComponent<PlayerStats>().getHealth() <= 0)
                        {
                            Debug.Log("Player died!");
                            monsterCurrentState = monsterState.foundNewPath;
                        }
                    }
                    //if aggro timer hits 0 then set monster state to foundNewPath where he will find another path to follow
                    if (aggroTimer <= 0)
                    {
                        aggroTimer = 6;
                        monsterCurrentState = monsterState.foundNewPath;
                    }
                    //if monster sees a player then set aggro timer to 8 and continue following player
                    if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out findPlayer, 10))
                    {
                        if (findPlayer.transform.tag == "Player")
                        {
                            aggroTimer = 8;
                        }
                    }
                    break;
                }
                //find another path for monster once monster finds a path point
            case (monsterState.foundNewPath):
                {
                    indexOfPath = Random.Range(0, monsterPaths.Length);
                    monster.SetDestination(monsterPaths[indexOfPath].transform.position);
                    chosenPath = monsterPaths[indexOfPath];
                    monsterCurrentState = monsterState.onPath;
                    break;
                }
        }
    }
}